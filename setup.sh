#!/usr/bin/env bash
distribution=$(lsb_release --id)
version=$(lsb_release -cs)
gitRepo="https://gitlab.com/vas213sd97/control-agent"
# ubuntu Setup
if [[ "$distribution" == *"Ubuntu"* ]]; then
  docker_install_url=https://download.docker.com/linux/ubuntu
elif [[ $distribution == *"Debian"* ]]; then
  docker_install_url=https://download.docker.com/linux/debian
else
  # No matching version found
  echo "Install for system $distribution, $version is not supported"
  exit 1
fi

sudo apt update -qqy \
  && sudo apt install --no-install-recommends --no-install-suggests -y make apt-transport-https ca-certificates curl gpg gnupg lsb-release git \
  && sudo apt autoremove --purge -y apache2 nginx

# https://docs.docker.com/engine/install
sudo mkdir -p /etc/apt/keyrings && sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /etc/apt/keyrings/docker.gpg
sudo echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] $docker_install_url \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list >/dev/null

sudo apt update -qqy && sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin


if [[ -d "/srv/control-agent/" ]]
then
  echo "Repository exists Removing..."
  sudo rm -rf /srv/control-agent/
fi
echo "Repository not exists Cloning...  ${gitRepo}"
sudo git clone ${gitRepo} /srv/control-agent


# create shared volume data directory for containers
sudo mkdir -p /srv/data/{nginx,letsencrypt,tldextract} &&
  sudo mkdir -p /srv/data/nginx/{ssl,upstreams,conf.d} &&
  sudo chown -R 1000:1000 /srv/data/

sudo /srv/control-agent/re-configure.sh $1