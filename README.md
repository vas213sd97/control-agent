# Tool that allow you auto generate letsencrypt certificates and Nginx configs.

#### Project commands

| Command     | Args |                Description                |
|:------------|:----:|:-----------------------------------------:|
| run         |  ✅   |              run application              |
| setup       |  ✅   |         setup nginx base configs          |
| sync        |  ❌   | sync exists certificates to nginx configs | 
| acme_auth   |  ❌   |        letsencrypt acme auth hook.        | 
| acme_deploy |  ❌   |       etsencrypt acme deploy hook.        | 